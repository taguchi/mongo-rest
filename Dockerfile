# taguchi/mongo-rest
FROM dockerfile/nodejs
MAINTAINER taguch1

# Install mongo-rest
RUN \
  git clone https://gitlab.com/taguchi/mongo-rest.git /mongo-rest && \
  cd /mongo-rest && npm install

EXPOSE 8080
CMD ["/mongo-rest/bin/docker-entry.sh"]
