var express = require("express");
var mongoskin = require("mongoskin");
var bodyParser = require("body-parser");
var morgan = require("morgan");

app = express();
app.use(morgan('combined'));
app.use(bodyParser.json({limit: 1024 * 1024 * 1024 * 15}));

app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, DELETE, OPTIONS");
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  return next();
});

var db = {};

app.param("db_name", function(req, res, next, db_name) {
  var host, port;
  req.db_name = db_name;
  host = process.env.MONGO_HOST || "localhost";
  port = process.env.MONGO_PORT || "27017";
  if (process.env.MONGO_DB && (process.env.MONGO_DB !== db_name)) {
    return res.status(404).send("/" + process.env.MONGO_DB + "/:collection_name/messages");
  }
  if (!db[db_name]) {
    db[db_name] = mongoskin.db("mongodb://@" + host + ":" + port + "/" + db_name, {
      safe: true
    });
  }
  return next();
});

app.param("collection_name", function(req, res, next, collection_name) {
  req.collection = db[req.db_name].collection(collection_name);
  return next();
});

app.get("/", function(req, res, next) {
  return res.status(404).send("/:database_name/:collection_name/messages");
});

app.get("/:db_name", function(req, res, next) {
  return res.status(404).send("/" + req.db_name + "/:collection_name/messages");
});

app.get("/:db_name/:collection_name", function(req, res, next) {
  return req.collection.find({}, {
    limit: 10,
    sort: {
      _id: -1
    }
  }).toArray(function(e, results) {
    if (e) {
      return next(e);
    }
    return res.send(results);
  });
});

app.get("/:db_name/:collection_name/:id", function(req, res, next) {
  return req.collection.findById(req.params.id, function(e, result) {
    if (e) {
      return next(e);
    }
    return res.send(result);
  });
});

app.post("/:db_name/:collection_name", function(req, res, next) {
  console.log(req.body);
  return req.collection.insert(req.body, {}, function(e, results) {
    if (e) {
      return next(e);
    }
    return res.send(results[0]);
  });
});

app.put("/:db_name/:collection_name/:id", function(req, res, next) {
  delete req.body._id;
  return req.collection.updateById(req.params.id, {
    $set: req.body
  }, {
    safe: true,
    multi: false
  }, function(e, result) {
    if (e) {
      return next(e);
    }
    return res.send((result === 1 ? {
      msg: "success"
    } : {
      msg: "error"
    }));
  });
});

app["delete"]("/:db_name/:collection_name/:id", function(req, res, next) {
  return req.collection.removeById(req.params.id, function(e, result) {
    if (e) {
      return next(e);
    }
    return res.send((result === 1 ? {
      msg: "success"
    } : {
      msg: "error"
    }));
  });
});

module.exports = app;
