# mongo-rest

## install

    npm install

## usage

    ./bin/mongo-rest -h

    usage: mongo-rest [options]

    options:
      -p                 Port [8080]
      -H --mongo-host    Mongo Host [localhost]
      -P --mongo-port    Mongo Port [27017]
      -d --mongo-db      Mongo DB Name

## sample

    docker run --name mongo -d -p 27017:27017 mongo
    ./bin/mongo-rest -H `boot2docker ip` 

    curl -X POST \
        -H "Accept: application/json" \
        -H "Content-type: application/json" \
        -d '{"first_name":"yamada","tarou":"lastname","email":"tarou@exsample.com"}'  \
        "http://localhost:8080/sample/users"

    curl "http://localhost:8080/sample/users/#{_id}"

    curl -X PUT \
      -H "Accept: application/json" \
      -H "Content-type: application/json"  \
      -d "{\"email\":\"tarou@example.com\"}" \
      "http://localhost:8080/sample/users/#{_id}"
    
    curl "http://localhost:8080/sample/users"
    
    curl -X DELETE "http://localhost:8080/sample/users/#{_id}"
